package com.rollong.auth.common.exception

import com.rollong.common.exception.auth.UnauthorizedException

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/04 00:20
 * @project rollong-auth
 * @filename NamespaceNotFound.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.auth.common.exception
 * @description
 */
class NamespaceNotFound(namespace: String) : UnauthorizedException(
    message = "namespace=${namespace}未找到"
) {
  override var errCode = Constant.NAMESPACE_NOT_FOUND
}