package com.rollong.auth.common

import java.io.Serializable

data class UserIdentifier(
    var userId: String = "",
    var userType: String = ""
) : Serializable