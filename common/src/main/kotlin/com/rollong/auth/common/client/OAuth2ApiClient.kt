package com.rollong.auth.common.client

import com.rollong.auth.common.api.LoginResponse
import com.rollong.auth.common.oauth2.api.OAuth2API
import com.rollong.auth.common.oauth2.authorizationCode.AuthByAuthorizationCodeRequest
import com.rollong.auth.common.oauth2.authorizationCode.GetAuthorizationCodeRequest
import com.rollong.common.exception.NotFoundException
import com.rollong.common.response.util.getOrThrow

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2021-01-05 13:45
 * @project rollong-auth
 * @filename OAuth2ApiClient.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.auth.common.client
 * @description
 */
class OAuth2ApiClient(
    val namespace: String? = null,
    private val oauth2Api: OAuth2API
) {

  fun generateAuthorizationCode(
      request: GetAuthorizationCodeRequest
  ): String {
    return this.oauth2Api.generateAuthorizationCode(this.namespace, request).getOrThrow().orElseThrow { NotFoundException() }
  }

  fun loginByAuthorizationCode(
      request: AuthByAuthorizationCodeRequest
  ): LoginResponse {
    return this.oauth2Api.loginByAuthorizationCode(this.namespace, request).getOrThrow().orElseThrow { NotFoundException() }
  }

}