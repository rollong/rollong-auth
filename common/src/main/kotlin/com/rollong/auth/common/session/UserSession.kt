package com.rollong.auth.common.session

import com.rollong.auth.common.UserIdentifier
import java.io.Serializable

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2021-01-03 18:23
 * @project rollong-auth
 * @filename UserSession.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.auth.common.session
 * @description
 */
data class UserSession(
    var id: String = "",
    var user: UserIdentifier = UserIdentifier(),
    var scope: Collection<String>? = null,
    var loginContext: LoginContext? = null,
    var loginAt: Long = 0L,
    var logoutAt: Long? = null,
    var lastActivity: Long = 0L,
    var content: MutableMap<String, String>? = null
) : Serializable