package com.rollong.auth.common.oauth2.authorizationCode

import com.rollong.auth.common.session.LoginContext

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/03 22:35
 * @project rollong-auth
 * @filename AuthByAuthorizationCodeRequest.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.auth.common.oauth2.AuthorizationCode
 * @description
 */
data class AuthByAuthorizationCodeRequest(
    var clientId: String? = null,
    var clientSecret: String? = null,
    var code: String = "",
    var loginContext: LoginContext? = null
)