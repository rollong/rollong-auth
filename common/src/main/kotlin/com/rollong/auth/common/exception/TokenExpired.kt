package com.rollong.auth.common.exception

import com.rollong.common.exception.auth.UnauthorizedException

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/04 01:08
 * @project rollong-auth
 * @filename TokenExpired.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.auth.common.exception
 * @description
 */
class TokenExpired : UnauthorizedException(
    message = "token已过期或失效"
) {
  override var errCode = Constant.TOKEN_EXPIRED
}