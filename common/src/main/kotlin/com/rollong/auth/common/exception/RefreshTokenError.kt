package com.rollong.auth.common.exception

import com.rollong.common.exception.auth.UnauthorizedException

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2021-01-04 14:03
 * @project rollong-auth
 * @filename RefreshTokenError.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.auth.common.exception
 * @description
 */
class RefreshTokenError : UnauthorizedException(
    message = "refresh_token不正确"
) {
  override var errCode = Constant.REFRESH_TOKEN_ERROR
}