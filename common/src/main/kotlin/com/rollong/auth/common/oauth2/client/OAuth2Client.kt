package com.rollong.auth.common.oauth2.client

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/03 22:30
 * @project rollong-auth
 * @filename OAuthClient.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.auth.common.oauth2
 * @description
 */
data class OAuth2Client(
    var id: String = "",
    var secret: String = ""
)