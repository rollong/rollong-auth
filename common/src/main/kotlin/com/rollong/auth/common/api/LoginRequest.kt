package com.rollong.auth.common.api

import com.rollong.auth.common.UserIdentifier
import com.rollong.auth.common.session.LoginContext

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2021-01-03 18:33
 * @project rollong-auth
 * @filename LoginRequest.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.auth.common.api
 * @description
 */
class LoginRequest(
    var userIdentifier: UserIdentifier = UserIdentifier(),
    var scope: Collection<String>? = null,
    var loginContext: LoginContext? = null,
    var session: MutableMap<String, String>? = null
)