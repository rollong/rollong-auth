package com.rollong.auth.common.exception

import com.rollong.common.exception.auth.UnauthorizedException

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2021-01-04 15:13
 * @project rollong-auth
 * @filename WrongAuthCode.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.auth.common.exception
 * @description
 */
class WrongAuthCode : UnauthorizedException(
    message = "认证用code不正确"
) {
  override var errCode = Constant.OAUTH2_WRONG_AUTH_CODE
}