package com.rollong.auth.common.client

import com.rollong.auth.common.api.*
import com.rollong.auth.common.session.UserSession
import com.rollong.common.exception.NotFoundException
import com.rollong.common.exception.auth.UnauthorizedException
import com.rollong.common.response.util.getOrThrow

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2021-01-05 11:59
 * @project rollong-auth
 * @filename AuthClient.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.auth.common.client
 * @description
 */
class AuthClient(
    val namespace: String? = null,
    private val authApi: AuthAPI
) {

  fun login(
      request: LoginRequest
  ): LoginResponse {
    return this.authApi.login(namespace = this.namespace, request = request).getOrThrow{ UnauthorizedException() }.orElseThrow { NotFoundException() }
  }

  fun auth(
      request: AuthRequest
  ): UserSession {
    return this.authApi.auth(namespace = this.namespace, request = request).getOrThrow{ UnauthorizedException() }.orElseThrow { NotFoundException() }
  }

  fun refresh(
      request: RefreshRequest
  ): LoginResponse {
    return this.authApi.refresh(namespace = this.namespace, request = request).getOrThrow { UnauthorizedException() }.orElseThrow { NotFoundException() }
  }

  fun logout(
      request: AuthRequest
  ): Boolean {
    return this.authApi.logout(namespace = this.namespace, request = request).getOrThrow().orElseThrow { NotFoundException() }
  }

  fun getSessions(
      request: GetSessionsRequest
  ): List<UserSession> {
    return this.authApi.getSessions(namespace = this.namespace, request = request).getOrThrow()
  }

  fun revokeSessions(
      request: RevokeSessionsRequest
  ): Map<String, Boolean> {
    return this.authApi.revokeSessions(namespace = this.namespace, request = request).getOrThrow().orElseThrow { NotFoundException() }
  }

}