package com.rollong.auth.common

interface HasPassword {
  var password: String
  var rawPassword: String
  fun checkPassword(rawPassword: String): Boolean
}