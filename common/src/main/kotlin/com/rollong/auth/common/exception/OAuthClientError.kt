package com.rollong.auth.common.exception

import com.rollong.common.exception.auth.UnauthorizedException

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2021-01-04 15:15
 * @project rollong-auth
 * @filename OAuthClientError.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.auth.common.exception
 * @description
 */
class OAuthClientError : UnauthorizedException(
    message = "clientId/clientSecret不正确"
) {
  override var errCode = Constant.OAUTH2_WRONG_CLIENT
}