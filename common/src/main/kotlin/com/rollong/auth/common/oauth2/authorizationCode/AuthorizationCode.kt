package com.rollong.auth.common.oauth2.authorizationCode

import com.rollong.auth.common.UserIdentifier
import java.io.Serializable

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/03 22:45
 * @project rollong-auth
 * @filename AuthorizationCode.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.auth.common.oauth2.authorizationCode
 * @description
 */
data class AuthorizationCode(
    var code: String = "",
    var clientId: String? = null,
    var scope: Collection<String>? = null,
    var userIdentifier: UserIdentifier = UserIdentifier(),
    var expiresAt: Long = 0L
) : Serializable