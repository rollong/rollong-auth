package com.rollong.auth.common.oauth2.authorizationCode

import com.rollong.auth.common.UserIdentifier

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/03 22:32
 * @project rollong-auth
 * @filename GetAuthorizationCodeRequest.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.auth.common.oauth2.AuthorizationCode
 * @description
 */
data class GetAuthorizationCodeRequest(
    var clientId: String? = null,
    var userIdentifier: UserIdentifier = UserIdentifier(),
    var scope: Collection<String>? = null,
    var expiresInSeconds: Int = 300
)