package com.rollong.auth.common.token

import java.io.Serializable

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2021-01-03 18:10
 * @project rollong-auth
 * @filename AuthToken.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.auth.common.token
 * @description
 */
data class Token(
    var id: String = "",
    var expiresAt: Long = 0L,
    var refreshToken: String? = null,
    var refreshTokenExpiresAt: Long? = null,
    var sessionId: String = ""
) : Serializable