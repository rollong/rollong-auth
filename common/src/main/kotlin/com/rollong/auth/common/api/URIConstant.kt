package com.rollong.auth.common.api

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/11 13:07
 * @project rollong-auth
 * @filename Constant.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.auth.common.api
 * @description
 */
object URIConstant {

  const val API_BASE = "/api/v1"

  const val LOGIN = "$API_BASE/auth/login"

  const val AUTH = "$API_BASE/auth/auth"

  const val REFRESH = "$API_BASE/auth/refresh"

  const val LOGOUT = "$API_BASE/auth/logout"

  const val GET_SESSIONS = "$API_BASE/auth/sessions"

  const val REVOKE_SESSIONS = "$API_BASE/auth/sessions/revoke"

  const val OAUTH2_GENERATE_CODE = "$API_BASE/oauth2/authorization_code/generate"

  const val OAUTH2_AUTH_BY_CODE = "$API_BASE/authorization_code/auth"
}