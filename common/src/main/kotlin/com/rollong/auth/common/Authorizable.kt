package com.rollong.auth.common

interface Authorizable {
  val userIdentifier: UserIdentifier
}