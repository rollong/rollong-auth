package com.rollong.auth.common.api

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2021-01-03 18:43
 * @project rollong-auth
 * @filename LoginResponse.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.auth.common.api
 * @description
 */
data class LoginResponse(
    var scope: Collection<String>? = null,
    var accessToken: String = "",
    var accessTokenExpiresAt: Long = 0L,
    var refreshToken: String? = null,
    var refreshTokenExpiresAt: Long? = null
)