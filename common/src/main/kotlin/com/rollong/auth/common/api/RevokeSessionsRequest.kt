package com.rollong.auth.common.api

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2021-01-24 10:06
 * @project rollong-auth
 * @filename RevokeSessionsRequest.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.auth.common.api
 * @description
 */
data class RevokeSessionsRequest(
    var sessionIds: List<String> = listOf()
)