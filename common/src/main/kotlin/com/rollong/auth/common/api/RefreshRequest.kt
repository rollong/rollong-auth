package com.rollong.auth.common.api

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2021-01-03 18:55
 * @project rollong-auth
 * @filename RefreshRequest.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.auth.common.api
 * @description
 */
data class RefreshRequest(
    var accessToken: String = "",
    var refreshToken: String = ""
)