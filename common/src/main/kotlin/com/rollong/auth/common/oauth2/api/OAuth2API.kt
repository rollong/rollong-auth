package com.rollong.auth.common.oauth2.api

import com.rollong.auth.common.api.LoginResponse
import com.rollong.auth.common.api.URIConstant
import com.rollong.auth.common.oauth2.authorizationCode.AuthByAuthorizationCodeRequest
import com.rollong.auth.common.oauth2.authorizationCode.GetAuthorizationCodeRequest
import com.rollong.common.response.GenericResponse
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/03 22:48
 * @project rollong-auth
 * @filename OAuth2API.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.auth.common.oauth2
 * @description
 */
interface OAuth2API {

  @PostMapping(URIConstant.OAUTH2_GENERATE_CODE)
  fun generateAuthorizationCode(
      @RequestParam(required = false) namespace: String? = null,
      @RequestBody request: GetAuthorizationCodeRequest
  ): GenericResponse<String>

  @PostMapping(URIConstant.OAUTH2_AUTH_BY_CODE)
  fun loginByAuthorizationCode(
      @RequestParam(required = false) namespace: String? = null,
      @RequestBody request: AuthByAuthorizationCodeRequest
  ): GenericResponse<LoginResponse>
}