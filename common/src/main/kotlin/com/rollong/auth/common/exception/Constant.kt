package com.rollong.auth.common.exception

import com.rollong.common.exception.ExceptionConstant

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/04 00:19
 * @project rollong-auth
 * @filename Constant.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.auth.common.exception
 * @description
 */
object Constant {
  const val NAMESPACE_NOT_FOUND = "${ExceptionConstant.AUTH_UNAUTHORIZED}.namespace_not_found"
  const val TOKEN_EXPIRED = "${ExceptionConstant.AUTH_UNAUTHORIZED}.token_expired"
  const val WRONG_TYPE = "${ExceptionConstant.AUTH_UNAUTHORIZED}.wrong_token_type"
  const val REFRESH_TOKEN_ERROR = "${ExceptionConstant.AUTH_UNAUTHORIZED}.refresh_token_error"
  const val OAUTH2_WRONG_AUTH_CODE = "${ExceptionConstant.AUTH_UNAUTHORIZED}.oauth2.code"
  const val OAUTH2_WRONG_CLIENT = "${ExceptionConstant.AUTH_UNAUTHORIZED}.oauth2.client"
  const val JWP_PARSE_ERROR = "${ExceptionConstant.AUTH_UNAUTHORIZED}.jwt.parse.error"
}