package com.rollong.auth.common.api

import com.rollong.auth.common.UserIdentifier

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2021-01-24 10:02
 * @project rollong-auth
 * @filename GetSessionsRequest.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.auth.common.api
 * @description
 */
data class GetSessionsRequest(
    var userIdentifier: UserIdentifier = UserIdentifier(),
    var clientName: String? = null
)