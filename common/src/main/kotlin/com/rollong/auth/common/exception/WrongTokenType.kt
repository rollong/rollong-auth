package com.rollong.auth.common.exception

import com.rollong.common.exception.auth.UnauthorizedException

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/04 01:23
 * @project rollong-auth
 * @filename WrongTokenType.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.auth.common.exception
 * @description
 */
class WrongTokenType : UnauthorizedException(
    message = "错误的token类型"
) {
  override var errCode = Constant.WRONG_TYPE
}