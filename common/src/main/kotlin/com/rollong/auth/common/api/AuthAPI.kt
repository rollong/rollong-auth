package com.rollong.auth.common.api

import com.rollong.auth.common.session.UserSession
import com.rollong.common.response.GenericPageableResponse
import com.rollong.common.response.GenericResponse
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam

interface AuthAPI {
  @PostMapping(URIConstant.LOGIN)
  fun login(
      @RequestParam(required = false) namespace: String? = null,
      @RequestBody request: LoginRequest
  ): GenericResponse<LoginResponse>

  @PostMapping(URIConstant.AUTH)
  fun auth(
      @RequestParam(required = false) namespace: String? = null,
      @RequestBody request: AuthRequest
  ): GenericResponse<UserSession>

  @PostMapping(URIConstant.REFRESH)
  fun refresh(
      @RequestParam(required = false) namespace: String? = null,
      @RequestBody request: RefreshRequest
  ): GenericResponse<LoginResponse>

  @PostMapping(URIConstant.LOGOUT)
  fun logout(
      @RequestParam(required = false) namespace: String? = null,
      @RequestBody request: AuthRequest
  ): GenericResponse<Boolean>

  @PostMapping(URIConstant.GET_SESSIONS)
  fun getSessions(
      @RequestParam(required = false) namespace: String? = null,
      @RequestBody request: GetSessionsRequest
  ): GenericPageableResponse<UserSession>

  @PostMapping(URIConstant.REVOKE_SESSIONS)
  fun revokeSessions(
      @RequestParam(required = false) namespace: String? = null,
      @RequestBody request: RevokeSessionsRequest
  ): GenericResponse<Map<String, Boolean>>
}