package com.rollong.auth.common.session

import java.io.Serializable

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2021-01-03 18:22
 * @project rollong-auth
 * @filename SessionContext.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.auth.common.session
 * @description
 */
data class LoginContext(
    var clientIP: String? = null,
    var clientName: String? = null,
    var clientVersion: String? = null,
    var userAgent: String? = null,
    var other: MutableMap<String, Any?>? = null
) : Serializable