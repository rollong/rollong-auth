package com.rollong.auth.common.exception

import com.rollong.common.exception.auth.UnauthorizedException

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2020-08-21 18:56
 * @project common
 * @filename JwtParseFailedException.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.auth.jwt
 * @description
 */
class JwtParseFailedException(message: String) : UnauthorizedException(
    message = message
) {
  override var errCode = Constant.JWP_PARSE_ERROR
}