package com.rollong.auth.server.config

import com.rollong.auth.common.exception.NamespaceNotFound
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.cloud.context.config.annotation.RefreshScope
import org.springframework.stereotype.Component

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/04 00:14
 * @project rollong-auth
 * @filename AuthProperties.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.auth.server.config
 * @description
 */
@Component
@ConfigurationProperties(prefix = "auth")
@RefreshScope
data class AuthProperties(
    var default: String = Namespace.DEFAULT_NAMESPACE,
    var namespaces: MutableMap<String, Namespace> = mutableMapOf()
) {

  fun getNamespace(namespace: String? = null): String = namespace ?: this.default

  @Throws(NamespaceNotFound::class)
  fun getConfig(namespace: String): Namespace {
    return this.namespaces[namespace] ?: throw NamespaceNotFound(namespace)
  }
}