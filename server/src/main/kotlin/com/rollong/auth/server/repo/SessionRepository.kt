package com.rollong.auth.server.repo

import com.rollong.auth.common.UserIdentifier
import com.rollong.auth.common.session.UserSession
import java.util.*

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/04 00:02
 * @project rollong-auth
 * @filename SessionRepository.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.auth.server.repo
 * @description
 */
interface SessionRepository {
  fun save(namespace: String, session: UserSession)
  fun get(namespace: String, id: String): Optional<UserSession>
  fun search(namespace: String, userIdentifier: UserIdentifier): List<UserSession>
  fun revoke(namespace: String, id: String)
  fun touch(namespace: String, id: String)
}