package com.rollong.auth.server.api

import com.rollong.common.exception.BaseException
import com.rollong.common.response.GenericResponse
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ExceptionHandler
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/04 00:56
 * @project rollong-auth
 * @filename APIController.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.auth.server.api
 * @description
 */
@Component
abstract class APIController {

  companion object {
    private val logger = LoggerFactory.getLogger(APIController::class.java)
  }

  @ExceptionHandler(Throwable::class)
  fun exceptionHandler(e: Throwable, request: HttpServletRequest, response: HttpServletResponse): GenericResponse<*> {
    logger.error("requestURI=${request.requestURI}", e)
    return when (e) {
      is BaseException -> {
        response.status = HttpStatus.OK.value()
        GenericResponse(
            code = e.code,
            errCode = e.errCode,
            message = e.message ?: e.javaClass.name,
            items = null
        )
      }
      is MethodArgumentNotValidException -> {
        response.status = HttpStatus.BAD_REQUEST.value()
        GenericResponse(
            code = 400,
            errCode = "fail",
            message = e.message,
            items = null
        )
      }
      else -> {
        response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
        GenericResponse(
            code = 500,
            errCode = "fail",
            message = e.message ?: e.javaClass.name,
            items = null
        )
      }
    }
  }
}