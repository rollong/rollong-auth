package com.rollong.auth.server.api

import com.rollong.auth.common.api.*
import com.rollong.auth.common.session.UserSession
import com.rollong.auth.server.config.AuthProperties
import com.rollong.auth.server.service.TokenService
import com.rollong.common.response.GenericPageableResponse
import com.rollong.common.response.GenericResponse
import com.rollong.common.response.util.toPageableResponse
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.web.bind.annotation.RestController

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/03 23:38
 * @project rollong-auth
 * @filename AuthAPIController.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.auth.server.api
 * @description
 */
@Api(tags = ["auth"])
@RestController
class AuthAPIController(
    private val tokenService: TokenService,
    private val authProperties: AuthProperties
) : APIController(), AuthAPI {

  @ApiOperation(value = "login")
  override fun login(namespace: String?, request: LoginRequest): GenericResponse<LoginResponse> {
    val ns = this.authProperties.getNamespace(namespace)
    val config = this.authProperties.getConfig(namespace = ns)
    val session = this.tokenService.createSession(ns, request.userIdentifier, request.scope, request.loginContext, request.session)
    val issueResult = this.tokenService.issue(
        ns,
        session,
        config.key.keyPair,
        config.accessTokenExpiresInSeconds,
        config.refreshTokenExpiresInSeconds
    )
    return GenericResponse(
        items = LoginResponse(
            scope = session.scope,
            accessToken = issueResult.accessToken.token,
            accessTokenExpiresAt = issueResult.accessToken.expiresAt,
            refreshToken = issueResult.refreshToken?.token,
            refreshTokenExpiresAt = issueResult.refreshToken?.expiresAt
        )
    )
  }

  @ApiOperation(value = "auth")
  override fun auth(namespace: String?, request: AuthRequest): GenericResponse<UserSession> {
    val ns = this.authProperties.getNamespace(namespace)
    val config = this.authProperties.getConfig(namespace = ns)
    val token = this.tokenService.getToken(ns, request.accessToken, null, config.key.keyPair)
    val session = this.tokenService.getSession(ns, token)
    return GenericResponse(items = session)
  }

  @ApiOperation(value = "logout")
  override fun logout(namespace: String?, request: AuthRequest): GenericResponse<Boolean> {
    val ns = this.authProperties.getNamespace(namespace)
    val config = this.authProperties.getConfig(namespace = ns)
    val token = this.tokenService.getToken(ns, request.accessToken, null, config.key.keyPair)
    this.tokenService.revoke(ns, token)
    this.tokenService.revokeSession(ns, token)
    return GenericResponse(
        items = true
    )
  }

  @ApiOperation(value = "refresh")
  override fun refresh(namespace: String?, request: RefreshRequest): GenericResponse<LoginResponse> {
    val ns = this.authProperties.getNamespace(namespace)
    val config = this.authProperties.getConfig(namespace = ns)
    val token = this.tokenService.getToken(
        ns,
        request.accessToken,
        request.refreshToken,
        config.key.keyPair
    )
    val session = this.tokenService.getSession(ns, token)
    this.tokenService.revoke(ns, token)
    val issueResult = this.tokenService.issue(
        ns,
        session,
        config.key.keyPair,
        config.accessTokenExpiresInSeconds,
        config.refreshTokenExpiresInSeconds
    )
    return GenericResponse(
        items = LoginResponse(
            scope = session.scope,
            accessToken = issueResult.accessToken.token,
            accessTokenExpiresAt = issueResult.accessToken.expiresAt,
            refreshToken = issueResult.refreshToken?.token,
            refreshTokenExpiresAt = issueResult.refreshToken?.expiresAt
        )
    )
  }

  @ApiOperation(value = "getSessions")
  override fun getSessions(namespace: String?, request: GetSessionsRequest): GenericPageableResponse<UserSession> {
    val ns = this.authProperties.getNamespace(namespace)
    return this.tokenService.getSessions(
        namespace = ns,
        userIdentifier = request.userIdentifier,
        clientName = request.clientName
    ).toPageableResponse()
  }

  @ApiOperation(value = "revokeSessions")
  override fun revokeSessions(namespace: String?, request: RevokeSessionsRequest): GenericResponse<Map<String, Boolean>> {
    val ns = this.authProperties.getNamespace(namespace)
    val results = mutableMapOf<String, Boolean>()
    for (sessionId in request.sessionIds) {
      results[sessionId] = try {
        this.tokenService.revokeSession(namespace = ns, sessionId = sessionId)
        true
      } catch (e: Throwable) {
        false
      }
    }
    return GenericResponse(items = results)
  }
}