package com.rollong.auth.server.config

import com.rollong.auth.common.oauth2.client.OAuth2Client
import com.rollong.auth.jwt.keyPair.JwtKeyConfig

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2021-01-04 13:27
 * @project rollong-auth
 * @filename Namespace.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.auth.server.config
 * @description
 */
data class Namespace(
    var key: JwtKeyConfig = JwtKeyConfig(),
    var accessTokenExpiresInSeconds: Int = 3 * 24 * 3600,
    var refreshTokenExpiresInSeconds: Int? = null,
    var clients: MutableList<OAuth2Client>? = null,
    var idleTimeoutInSeconds: Int? = null
) {
  companion object {
    const val DEFAULT_NAMESPACE = "default"
  }
}