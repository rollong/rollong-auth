package com.rollong.auth.server.service

import com.rollong.auth.common.UserIdentifier
import com.rollong.auth.common.exception.RefreshTokenError
import com.rollong.auth.common.exception.TokenExpired
import com.rollong.auth.common.exception.WrongTokenType
import com.rollong.auth.common.session.LoginContext
import com.rollong.auth.common.session.UserSession
import com.rollong.auth.common.token.Token
import com.rollong.auth.jwt.token.JwtToken
import com.rollong.auth.jwt.token.JwtTokenContent
import com.rollong.auth.jwt.token.TokenType
import com.rollong.auth.jwt.util.JwtUtil
import com.rollong.auth.server.repo.SessionRepository
import com.rollong.auth.server.repo.TokenRepository
import org.springframework.stereotype.Service
import java.security.KeyPair

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2021-01-04 13:32
 * @project rollong-auth
 * @filename TokenService.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.auth.server.service
 * @description
 */
@Service
class TokenService(
    private val tokenRepository: TokenRepository,
    private val sessionRepository: SessionRepository
) {

  fun createSession(
      namespace: String,
      userIdentifier: UserIdentifier,
      scope: Collection<String>? = null,
      loginContext: LoginContext? = null,
      sessionContent: MutableMap<String, String>? = null
  ): UserSession {
    val now = System.currentTimeMillis()
    val session = UserSession(
        id = RandomCodeGenerator.make(),
        user = userIdentifier,
        scope = scope,
        loginContext = loginContext,
        loginAt = now,
        logoutAt = null,
        lastActivity = now,
        content = sessionContent
    )
    this.sessionRepository.save(namespace, session)
    return session
  }

  private fun issueAccessToken(
      userIdentifier: UserIdentifier,
      scope: Collection<String>? = null,
      session: UserSession,
      expiresAt: Long,
      keyPair: KeyPair
  ): JwtToken {
    return JwtUtil.issue(
        content = JwtTokenContent(
            jti = RandomCodeGenerator.make(),
            subject = userIdentifier.userId,
            userType = userIdentifier.userType,
            sessionId = session.id,
            scope = scope,
            expiresAt = expiresAt,
            tokenType = TokenType.AccessToken
        ),
        keyPair = keyPair
    )
  }

  private fun issueRefreshToken(
      userIdentifier: UserIdentifier,
      session: UserSession,
      expiresAt: Long,
      keyPair: KeyPair
  ): JwtToken {
    return JwtUtil.issue(
        content = JwtTokenContent(
            jti = RandomCodeGenerator.make(),
            subject = userIdentifier.userId,
            userType = userIdentifier.userType,
            sessionId = session.id,
            expiresAt = expiresAt,
            tokenType = TokenType.RefreshToken
        ),
        keyPair = keyPair
    )
  }

  fun issue(
      namespace: String,
      session: UserSession,
      keyPair: KeyPair,
      accessTokenExpiresInSeconds: Int,
      refreshTokenExpiresInSeconds: Int?
  ): IssueResult {
    val now = System.currentTimeMillis()
    val accessToken = this.issueAccessToken(
        userIdentifier = session.user,
        scope = session.scope,
        session = session,
        expiresAt = now + accessTokenExpiresInSeconds * 1000L,
        keyPair = keyPair
    )
    val refreshToken = refreshTokenExpiresInSeconds?.let {
      this.issueRefreshToken(
          userIdentifier = session.user,
          session = session,
          expiresAt = now + it * 1000L,
          keyPair = keyPair
      )
    }
    this.save(namespace, session, accessToken, refreshToken)
    return IssueResult(
        accessToken, refreshToken
    )
  }

  fun save(
      namespace: String,
      session: UserSession,
      accessToken: JwtToken,
      refreshToken: JwtToken? = null
  ): Token {
    val token = Token(
        id = accessToken.jti,
        sessionId = session.id,
        expiresAt = accessToken.expiresAt,
        refreshToken = refreshToken?.jti,
        refreshTokenExpiresAt = refreshToken?.expiresAt
    )
    this.tokenRepository.save(namespace, token)
    return token
  }

  @Throws(RefreshTokenError::class, WrongTokenType::class, TokenExpired::class)
  fun getToken(
      namespace: String,
      accessToken: String,
      refreshToken: String?,
      keyPair: KeyPair
  ): Token {
    val now = System.currentTimeMillis()
    val tokenContent = JwtUtil.parse(token = accessToken, keyPair = keyPair)
    if (tokenContent.expiresAt < now) {
      throw TokenExpired()
    }
    if (tokenContent.tokenType != TokenType.AccessToken) {
      throw WrongTokenType()
    }
    val refreshTokenContent = refreshToken?.let {
      val content = JwtUtil.parse(token = it, keyPair = keyPair)
      if (content.expiresAt < now) {
        throw TokenExpired()
      }
      if (content.tokenType != TokenType.RefreshToken) {
        throw WrongTokenType()
      }
      content
    }
    val token = this.tokenRepository.get(namespace, tokenContent.jti).orElseThrow { TokenExpired() }
    if (null != refreshTokenContent && refreshTokenContent.jti != token.refreshToken) {
      throw RefreshTokenError()
    }
    return token
  }

  fun revoke(namespace: String, token: Token) {
    this.tokenRepository.revoke(namespace, token.id)
  }

  fun revokeSession(namespace: String, token: Token) {
    this.revokeSession(namespace = namespace, sessionId = token.sessionId)
  }

  fun revokeSession(namespace: String, sessionId: String) {
    this.sessionRepository.revoke(namespace, sessionId)
  }

  fun getSessions(namespace: String, userIdentifier: UserIdentifier, clientName: String? = null): List<UserSession> {
    return this.sessionRepository.search(namespace, userIdentifier).filter {
      clientName == null || clientName == it.loginContext?.clientName
    }
  }

  fun getSession(namespace: String, token: Token, idleTimeoutInSeconds: Int? = null): UserSession {
    val session = this.sessionRepository.get(namespace = namespace, id = token.sessionId).orElseThrow {
      this.tokenRepository.revoke(namespace = namespace, id = token.id)
      TokenExpired()
    }
    val now = System.currentTimeMillis()
    if (idleTimeoutInSeconds != null) {
      if (now - session.lastActivity >= idleTimeoutInSeconds * 1000L) {
        this.revoke(namespace, token)
        this.sessionRepository.revoke(namespace, session.id)
        throw TokenExpired()
      }
    }
    this.sessionRepository.touch(namespace, session.id)
    return session
  }
}