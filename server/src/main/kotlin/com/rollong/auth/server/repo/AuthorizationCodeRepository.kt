package com.rollong.auth.server.repo

import com.rollong.auth.common.oauth2.authorizationCode.AuthorizationCode
import java.util.*

interface AuthorizationCodeRepository {
  fun save(namespace: String, code: AuthorizationCode)
  fun get(namespace: String, code: String): Optional<AuthorizationCode>
}