package com.rollong.auth.server.service

import com.rollong.auth.common.UserIdentifier
import com.rollong.auth.common.exception.WrongAuthCode
import com.rollong.auth.common.oauth2.authorizationCode.AuthorizationCode
import com.rollong.auth.server.repo.AuthorizationCodeRepository
import org.springframework.stereotype.Service

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2021-01-04 14:40
 * @project rollong-auth
 * @filename AuthorizationCodeService.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.auth.server.service
 * @description
 */
@Service
class AuthorizationCodeService(
    private val codeRepository: AuthorizationCodeRepository
) {

  fun generate(
      namespace: String,
      user: UserIdentifier,
      clientId: String? = null,
      expiresAt: Long,
      scope: Collection<String>? = null
  ): String {
    val code = AuthorizationCode(
        code = RandomCodeGenerator.make(),
        scope = scope,
        userIdentifier = user,
        clientId = clientId,
        expiresAt = expiresAt
    )
    this.codeRepository.save(namespace, code)
    return code.code
  }

  fun get(
      namespace: String,
      code: String
  ) = this.codeRepository.get(namespace, code).orElseThrow { WrongAuthCode() }
}