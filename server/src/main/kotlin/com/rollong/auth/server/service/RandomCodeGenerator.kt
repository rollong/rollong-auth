package com.rollong.auth.server.service

import com.rollong.common.util.random.toRandomString

object RandomCodeGenerator {

  private val pool = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890._-".toCharArray()

  fun make(length: Int = 63) = pool.toRandomString(length - 13) + System.currentTimeMillis()
}