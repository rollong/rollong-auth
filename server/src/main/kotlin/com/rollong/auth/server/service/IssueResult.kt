package com.rollong.auth.server.service

import com.rollong.auth.jwt.token.JwtToken

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2021-01-04 14:28
 * @project rollong-auth
 * @filename IssueResult.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.auth.server.service
 * @description
 */
data class IssueResult(
    val accessToken: JwtToken,
    val refreshToken: JwtToken?
)