package com.rollong.auth.server.repo

import com.rollong.auth.common.token.Token
import java.util.*

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/03 23:54
 * @project rollong-auth
 * @filename TokenRepository.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.auth.server.repo
 * @description
 */
interface TokenRepository {
  fun save(namespace: String, token: Token)
  fun get(namespace: String, id: String): Optional<Token>
  fun revoke(namespace: String, id: String)
}