package com.rollong.auth.server.api

import com.rollong.auth.common.api.LoginResponse
import com.rollong.auth.common.exception.OAuthClientError
import com.rollong.auth.common.oauth2.api.OAuth2API
import com.rollong.auth.common.oauth2.authorizationCode.AuthByAuthorizationCodeRequest
import com.rollong.auth.common.oauth2.authorizationCode.GetAuthorizationCodeRequest
import com.rollong.auth.server.config.AuthProperties
import com.rollong.auth.server.service.AuthorizationCodeService
import com.rollong.auth.server.service.TokenService
import com.rollong.common.response.GenericResponse
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.web.bind.annotation.RestController
import kotlin.math.max

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2021-01-04 14:38
 * @project rollong-auth
 * @filename OAuth2APIController.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.auth.server.api
 * @description
 */
@Api(tags = ["auth"])
@RestController
class OAuth2APIController(
    private val tokenService: TokenService,
    private val authProperties: AuthProperties,
    private val codeService: AuthorizationCodeService
) : APIController(), OAuth2API {

  @ApiOperation(value = "generate")
  override fun generateAuthorizationCode(namespace: String?, request: GetAuthorizationCodeRequest): GenericResponse<String> {
    val ns = this.authProperties.getNamespace(namespace)
    return GenericResponse(
        items = this.codeService.generate(
            namespace = ns,
            user = request.userIdentifier,
            clientId = request.clientId,
            expiresAt = System.currentTimeMillis() + max(request.expiresInSeconds, 1) * 1000L,
            scope = request.scope
        )
    )
  }

  @ApiOperation(value = "login by code")
  @Throws(OAuthClientError::class)
  override fun loginByAuthorizationCode(namespace: String?, request: AuthByAuthorizationCodeRequest): GenericResponse<LoginResponse> {
    val ns = this.authProperties.getNamespace(namespace)
    val config = this.authProperties.getConfig(namespace = ns)
    val code = this.codeService.get(ns, request.code)
    val clientId = code.clientId
    if (null != clientId) {
      val clientSecret = config.clients?.firstOrNull { it.id == clientId }?.secret ?: throw OAuthClientError()
      if (request.clientId != clientId || request.clientSecret != clientSecret) {
        throw OAuthClientError()
      }
    }
    val session = this.tokenService.createSession(namespace = ns, userIdentifier = code.userIdentifier, scope = code.scope, loginContext = request.loginContext)
    val issueResult = this.tokenService.issue(
        ns, session, config.key.keyPair, config.accessTokenExpiresInSeconds, config.refreshTokenExpiresInSeconds
    )
    return GenericResponse(
        items = LoginResponse(
            scope = session.scope,
            accessToken = issueResult.accessToken.token,
            accessTokenExpiresAt = issueResult.accessToken.expiresAt,
            refreshToken = issueResult.refreshToken?.token,
            refreshTokenExpiresAt = issueResult.refreshToken?.expiresAt
        )
    )
  }
}