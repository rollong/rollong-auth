#!/bin/bash

./gradlew :clean \
  :common:clean common:build \
  :jwt:clean jwt:build \
  :server:clean server:build \
  :server-impl:redis:clean :server-impl:redis:jib \
  :server-impl:standalone:clean :server-impl:standalone:jib
