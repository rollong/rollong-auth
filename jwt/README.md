1. OPENSSL生成RSA证书

```
openssl genrsa -out rsa_private_key.pem 2048
openssl rsa -in rsa_private_key.pem -pubout -out rsa_public_key.pem
```

1. 将.pem转为.der

```
openssl pkcs8 -topk8 -inform PEM -outform DER -in rsa_private_key.pem -out rsa_private_key.der -nocrypt
openssl pkcs8 -topk8 -inform PEM -outform DER -in rsa_public_key.pem -out rsa_public_key.der -nocrypt
```

1. base64 .der

```
openssl ecparam -name secp256k1 -genkey -out ec_private_key.pem
openssl ec -in ec_private_key.pem -pubout -out ec_public_key.pem
openssl pkcs8 -topk8 -inform PEM -outform DER -in ec_private_key.pem -out ec_private_key.der -nocrypt
openssl ec -inform PEM -pubin -outform DER -in ec_public_key.pem -out ec_public_key.der
```