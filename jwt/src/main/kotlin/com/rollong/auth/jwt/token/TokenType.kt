package com.rollong.auth.jwt.token

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2021-01-03 16:37
 * @project rollong-common
 * @filename TokenType.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.auth.jwt.data
 * @description
 */
enum class TokenType {
  AccessToken, RefreshToken, OneTimeToken
}