package com.rollong.auth.jwt.token

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2021-01-03 16:36
 * @project rollong-common
 * @filename TokenContent.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.auth.jwt.data
 * @description
 */
data class JwtTokenContent(
    val jti: String,
    val subject: String,
    val userType: String,
    val scope: Collection<String>? = null,
    val sessionId: String,
    val issuedAt: Long = System.currentTimeMillis(),
    val issuer: String = "default",
    val expiresAt: Long,
    val tokenType: TokenType = TokenType.AccessToken,
    val extra: Any? = null
)