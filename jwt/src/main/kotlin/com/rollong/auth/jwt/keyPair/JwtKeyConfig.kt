package com.rollong.auth.jwt.keyPair

import java.security.KeyPair
import java.security.KeyPairGenerator
import kotlin.properties.Delegates

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2020-08-23 0:28
 * @project common
 * @filename JwtConfig.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.auth.jwt
 * @description
 */
class JwtKeyConfig {

  private var _keyPair: KeyPair? = null

  val keyPair: KeyPair
    get() {
      if (null == this._keyPair) {
        if (this.random) {
          this._keyPair = KeyPairGenerator.getInstance(this.alg).generateKeyPair()
        } else {
          this._keyPair = KeyPairUtil.read(
              publicKey = this.publicKey.replace("\n", ""),
              privateKey = this.privateKey?.replace("\n", ""),
              algorithm = this.alg
          )
        }
      }
      return this._keyPair!!
    }


  var random: Boolean by Delegates.observable(false) { _, _, _ ->
    this._keyPair = null
  }

  var alg: String by Delegates.observable("EC") { _, _, _ ->
    this._keyPair = null
  }

  var publicKey: String by Delegates.observable("") { _, _, _ ->
    this._keyPair = null
  }

  var privateKey: String? by Delegates.observable(null) { _, _, _ ->
    this._keyPair = null
  }
}