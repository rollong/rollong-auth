package com.rollong.auth.jwt.token

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2021-01-03 16:13
 * @project rollong-common
 * @filename JwtToken.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.auth.jwt
 * @description
 */
data class JwtToken(
    val jti: String,
    val token: String,
    val expiresAt: Long
)