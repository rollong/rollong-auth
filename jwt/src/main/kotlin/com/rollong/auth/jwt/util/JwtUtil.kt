package com.rollong.auth.jwt.util

import com.rollong.auth.common.exception.JwtParseFailedException
import com.rollong.auth.jwt.token.JwtToken
import com.rollong.auth.jwt.token.JwtTokenContent
import com.rollong.auth.jwt.token.TokenType
import io.jsonwebtoken.JwtException
import io.jsonwebtoken.Jwts
import java.security.KeyPair
import java.util.*

object JwtUtil {

  const val CLAIM_KEY_USERTYPE = "user_type"
  const val CLAIM_KEY_SESSIONID = "session_id"
  const val CLAIM_KEY_SCOPE = "scope"
  const val CLAIM_KEY_TOKEN_TYPE = "token_type"
  const val CLAIM_KEY_EXTRA = "extra"

  @Throws(JwtParseFailedException::class)
  fun issue(
      content: JwtTokenContent,
      keyPair: KeyPair
  ): JwtToken {
    keyPair.private ?: throw JwtParseFailedException("没有PrivateKey,无法签发token")
    val token = Jwts.builder()
        .setSubject(content.subject)
        .claim(CLAIM_KEY_SCOPE, content.scope)
        .claim(CLAIM_KEY_SESSIONID, content.sessionId)
        .claim(CLAIM_KEY_USERTYPE, content.userType)
        .claim(CLAIM_KEY_TOKEN_TYPE, content.tokenType.name)
        .also {
          if (null != content.extra) {
            it.claim(CLAIM_KEY_EXTRA, content.extra)
          }
        }
        .setId(content.jti)
        .setIssuedAt(Date())
        .setNotBefore(Date(System.currentTimeMillis() - 5 * 60 * 1000L))
        .setExpiration(Date(content.expiresAt))
        .setIssuer(content.issuer)
        .signWith(keyPair.private)
        .compact()
    return JwtToken(
        jti = content.jti,
        token = token,
        expiresAt = content.expiresAt
    )
  }

  @Suppress("UNCHECKED_CAST")
  @Throws(JwtParseFailedException::class)
  fun parse(
      token: String,
      keyPair: KeyPair
  ): JwtTokenContent {
    val body = try {
      Jwts.parserBuilder()
          .setSigningKey(keyPair.public)
          .build()
          .parseClaimsJws(token)
    } catch (e: JwtException) {
      throw JwtParseFailedException("jwt解析失败 错误信息:${e.message}")
    }.body
    return JwtTokenContent(
        jti = body.id,
        subject = body.subject as String,
        userType = body[CLAIM_KEY_USERTYPE] as String,
        scope = body[CLAIM_KEY_SCOPE] as Collection<String>?,
        sessionId = body[CLAIM_KEY_SESSIONID] as String,
        issuedAt = body.issuedAt.time,
        issuer = body.issuer,
        expiresAt = body.expiration.time,
        tokenType = TokenType.valueOf(body[CLAIM_KEY_TOKEN_TYPE] as String),
        extra = body[CLAIM_KEY_EXTRA]
    )
  }
}