package com.rollong.auth.jwt.keyPair

import io.jsonwebtoken.SignatureAlgorithm
import io.jsonwebtoken.security.Keys
import java.security.KeyFactory
import java.security.KeyPair
import java.security.spec.PKCS8EncodedKeySpec
import java.security.spec.X509EncodedKeySpec
import java.util.*

object KeyPairUtil {

  /**
   * DER格式bytearray
   */
  fun read(publicKeyBytes: ByteArray,
           privateKeyBytes: ByteArray? = null,
           algorithm: String = "RSA"
  ): KeyPair {
    val publicKey = publicKeyBytes.let {
      val spec = X509EncodedKeySpec(it)
      val kf = KeyFactory.getInstance(algorithm)
      kf.generatePublic(spec)
    }
    val privateKey = privateKeyBytes?.let {
      val spec = PKCS8EncodedKeySpec(it)
      val kf = KeyFactory.getInstance(algorithm)
      kf.generatePrivate(spec)
    }
    return KeyPair(publicKey, privateKey)
  }

  /**
   * DER格式base64
   */
  fun read(
      publicKey: String,
      privateKey: String? = null,
      algorithm: String = "RSA"
  ) = read(
      publicKeyBytes = Base64.getDecoder().decode(publicKey),
      privateKeyBytes = privateKey?.let { Base64.getDecoder().decode(it) },
      algorithm = algorithm
  )

  fun generate(alg: SignatureAlgorithm): KeyPair {
    return Keys.keyPairFor(alg)
  }
}