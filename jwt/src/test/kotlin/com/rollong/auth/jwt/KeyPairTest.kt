package com.rollong.auth.jwt

import com.rollong.auth.jwt.keyPair.JwtKeyConfig
import com.rollong.auth.jwt.token.JwtTokenContent
import com.rollong.auth.jwt.util.JwtUtil
import org.junit.Test

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/08 17:52
 * @project rollong-auth
 * @filename KeyPairTest.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.auth.jwt
 * @description
 */
class TokenTest {

  @Test
  fun assign() {
    val config = JwtKeyConfig()
        .also { it.privateKey = "MIGEAgEAMBAGByqGSM49AgEGBSuBBAAKBG0wawIBAQQg+SdI/EZY/XqPGEoLJppRSptbvKr7MWvyndyeLMITJR+hRANCAAQ3yPXypgrDGi1DzOPGAJlcLPyfPBW9BR3WroQw+OO2g3v6ehA/NiyY9TFfWwlKb01j1ssWO+hNk/EWLFFn+siC" }
        .also { it.publicKey = "MFYwEAYHKoZIzj0CAQYFK4EEAAoDQgAEN8j18qYKwxotQ8zjxgCZXCz8nzwVvQUd1q6EMPjjtoN7+noQPzYsmPUxX1sJSm9NY9bLFjvoTZPxFixRZ/rIgg==" }
        .also { it.random = false }
    val token = JwtUtil.issue(
        content = JwtTokenContent(
            jti = "1",
            subject = "2",
            userType = "3",
            sessionId = "4",
            scope = listOf("test1", "test2"),
            expiresAt = System.currentTimeMillis() + 60000L
        ),
        keyPair = config.keyPair
    )
    val content = JwtUtil.parse(token = token.token,keyPair = config.keyPair)
    assert(true == content.scope?.contains("test2"))
  }
}