package com.rollong.auth.server.impl.redis.repo

import com.rollong.auth.common.oauth2.authorizationCode.AuthorizationCode
import com.rollong.auth.server.repo.AuthorizationCodeRepository
import com.rollong.common.json.JSONUtil
import com.rollong.common.json.parseJsonObject
import org.springframework.data.redis.core.RedisTemplate
import org.springframework.stereotype.Component
import java.util.*
import java.util.concurrent.TimeUnit
import javax.annotation.Resource
import kotlin.math.max

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2021-01-04 18:53
 * @project rollong-auth
 * @filename RedisAuthorizationCodeRepositoryImpl.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.auth.server.impl.redis.repo
 * @description
 */
@Component
class RedisAuthorizationCodeRepositoryImpl : AuthorizationCodeRepository {

  @Resource
  lateinit var redisTemplate: RedisTemplate<RedisKey, String>

  override fun get(namespace: String, code: String): Optional<AuthorizationCode> {
    val key = RedisKey(namespace, code, RedisKey.TYPE_AUTH_CODE)
    val ops = this.redisTemplate.opsForValue()
    val result = ops.get(key)?.parseJsonObject<AuthorizationCode>()
    if (result != null) {
      this.redisTemplate.delete(key)
      if (result.expiresAt > System.currentTimeMillis()) {
        return Optional.of(result)
      }
    }
    return Optional.empty()
  }

  override fun save(namespace: String, code: AuthorizationCode) {
    val key = RedisKey(namespace, code.code, RedisKey.TYPE_AUTH_CODE)
    val ops = this.redisTemplate.opsForValue()
    ops.set(key, JSONUtil.toJSONString(code), max(code.expiresAt - System.currentTimeMillis(), 1000L), TimeUnit.MILLISECONDS)
  }
}