package com.rollong.auth.server.impl.redis.repo

import com.rollong.auth.common.UserIdentifier
import com.rollong.auth.common.session.UserSession
import com.rollong.auth.server.repo.SessionRepository
import com.rollong.common.json.JSONUtil
import com.rollong.common.json.parseJsonObject
import org.springframework.data.redis.core.RedisTemplate
import org.springframework.stereotype.Component
import java.util.*
import javax.annotation.Resource

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/05 00:07
 * @project rollong-auth
 * @filename RedisSessionRepositoryImpl.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.auth.server.impl.redis.repo
 * @description
 */
@Component
class RedisSessionRepositoryImpl : SessionRepository {

  @Resource
  lateinit var redisTemplate: RedisTemplate<RedisKey, String>

  override fun get(namespace: String, id: String): Optional<UserSession> {
    val key = RedisKey(namespace, id, RedisKey.TYPE_SESSION)
    val session = this.redisTemplate.opsForValue().get(key)?.parseJsonObject<UserSession>()
    if (null != session) {
      if (session.logoutAt == null) {
        return Optional.of(session)
      } else {
        this.redisTemplate.delete(key)
      }
    }
    return Optional.empty()
  }

  override fun revoke(namespace: String, id: String) {
    val key = RedisKey(namespace, id, RedisKey.TYPE_SESSION)
    val session = this.redisTemplate.opsForValue().get(key)?.parseJsonObject<UserSession>()
    this.redisTemplate.delete(key)
    if (null != session) {
      val sessionIds = this.getSessionIds(namespace = namespace, userIdentifier = session.user)
      if (null != sessionIds) {
        sessionIds.toMutableList().remove(id)
        this.saveSessionIds(namespace = namespace, userIdentifier = session.user, sessionIds = sessionIds)
      }
    }
  }

  override fun save(namespace: String, session: UserSession) {
    val key = RedisKey(namespace, session.id, RedisKey.TYPE_SESSION)
    val sessionId = session.id
    val userIdentifier = session.user
    val sessionIds = this.getSessionIds(namespace, userIdentifier) ?: listOf()
    if (!sessionIds.contains(sessionId)) {
      this.saveSessionIds(namespace, userIdentifier, sessionIds.toMutableList().apply { this.add(sessionId) })
    }
    this.redisTemplate.opsForValue().set(key, JSONUtil.toJSONString(session))
  }

  override fun touch(namespace: String, id: String) {
    val session = this.get(namespace, id).orElseGet { null }
    if (null != session) {
      session.lastActivity = System.currentTimeMillis()
      this.save(namespace, session)
    }
  }

  override fun search(namespace: String, userIdentifier: UserIdentifier): List<UserSession> {
    return this.getSessionIds(namespace = namespace, userIdentifier = userIdentifier)?.mapNotNull {
      this.get(namespace = namespace, id = it).orElseGet { null }
    } ?: listOf()
  }

  private fun getSessionIds(namespace: String, userIdentifier: UserIdentifier): List<String>? {
    return this.redisTemplate.opsForHash<String, String>()
        .get(this.userIdx(namespace), JSONUtil.toJSONString(userIdentifier))
        ?.let { JSONUtil.parseArray<String>(it).filterNotNull() }
  }

  private fun saveSessionIds(namespace: String, userIdentifier: UserIdentifier, sessionIds: List<String>) {
    if (sessionIds.isEmpty()) {
      this.redisTemplate.opsForHash<String, String>().delete(this.userIdx(namespace), JSONUtil.toJSONString(userIdentifier))
    } else {
      this.redisTemplate.opsForHash<String, String>().put(this.userIdx(namespace), JSONUtil.toJSONString(userIdentifier), JSONUtil.toJSONString(sessionIds))
    }
  }

  private fun userIdx(namespace: String): RedisKey {
    return RedisKey(
        namespace = namespace,
        id = "",
        type = RedisKey.TYPE_SESSION_USER_IDX
    )
  }
}