package com.rollong.auth.server.impl.redis.config

import com.rollong.auth.server.impl.redis.repo.RedisKey
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.redis.connection.RedisConnectionFactory
import org.springframework.data.redis.core.RedisTemplate
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer
import org.springframework.data.redis.serializer.StringRedisSerializer

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/05 01:25
 * @project rollong-auth
 * @filename RedisConfig.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.auth.server.impl.redis.config
 * @description
 */
@Configuration
class RedisConfig {

  @Bean
  @Throws(Exception::class)
  fun redisTemplate(redisConnectionFactory: RedisConnectionFactory): RedisTemplate<*, *> {
    val redisTemplate: RedisTemplate<*, *> = RedisTemplate<Any?, Any?>()
    redisTemplate.setConnectionFactory(redisConnectionFactory)
    redisTemplate.keySerializer = Jackson2JsonRedisSerializer(RedisKey::class.java)
    redisTemplate.valueSerializer = StringRedisSerializer()
    redisTemplate.hashKeySerializer = StringRedisSerializer()
    redisTemplate.hashValueSerializer = StringRedisSerializer()
    redisTemplate.afterPropertiesSet()
    return redisTemplate
  }
}