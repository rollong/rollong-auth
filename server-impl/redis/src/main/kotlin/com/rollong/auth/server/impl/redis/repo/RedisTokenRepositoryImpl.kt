package com.rollong.auth.server.impl.redis.repo

import com.rollong.auth.common.token.Token
import com.rollong.auth.server.repo.TokenRepository
import com.rollong.common.json.JSONUtil
import com.rollong.common.json.parseJsonObject
import org.springframework.data.redis.core.RedisTemplate
import org.springframework.stereotype.Component
import java.util.*
import java.util.concurrent.TimeUnit
import javax.annotation.Resource
import kotlin.math.max

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/05 00:24
 * @project rollong-auth
 * @filename RedisTokenRepositoryImpl.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.auth.server.impl.redis.repo
 * @description
 */
@Component
class RedisTokenRepositoryImpl : TokenRepository {

  @Resource
  lateinit var redisTemplate: RedisTemplate<RedisKey, String>

  override fun get(namespace: String, id: String): Optional<Token> {
    val key = RedisKey(namespace, id, RedisKey.TYPE_TOKEN)
    val token = this.redisTemplate.opsForValue().get(key)?.parseJsonObject<Token>()
    if (null != token) {
      if (token.expiresAt > System.currentTimeMillis()) {
        return Optional.of(token)
      }
      this.redisTemplate.delete(key)
    }
    return Optional.empty()
  }

  override fun revoke(namespace: String, id: String) {
    val key = RedisKey(namespace, id, RedisKey.TYPE_TOKEN)
    this.redisTemplate.delete(key)
  }

  override fun save(namespace: String, token: Token) {
    val key = RedisKey(namespace, token.id, RedisKey.TYPE_TOKEN)
    this.redisTemplate.opsForValue().set(key, JSONUtil.toJSONString(token), max(token.expiresAt - System.currentTimeMillis(), 1000L), TimeUnit.MILLISECONDS)
  }
}