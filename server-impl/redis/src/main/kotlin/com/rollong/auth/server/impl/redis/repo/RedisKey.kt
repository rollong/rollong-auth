package com.rollong.auth.server.impl.redis.repo

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/04 23:30
 * @project rollong-auth
 * @filename RedisKey.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.auth.server.impl.redis.repo
 * @description
 */
data class RedisKey(
    var namespace: String = "",
    var id: String = "",
    var type: String = ""
) {
  companion object {
    const val TYPE_AUTH_CODE = "auth_code"
    const val TYPE_TOKEN = "token"
    const val TYPE_SESSION = "session"
    const val TYPE_SESSION_USER_IDX = "session_user_idx"
  }
}