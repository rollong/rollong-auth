package com.rollong.auth.server.impl.redis

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.ComponentScan

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2021-01-04 16:08
 * @project rollong-auth
 * @filename ServerApplication.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.auth.server.impl.standalone
 * @description
 */
@SpringBootApplication
@ComponentScan(basePackages = ["com.rollong.auth.server"])
class RedisServerApplication

fun main(args: Array<String>) {
  runApplication<RedisServerApplication>(*args)
}