package com.rollong.auth.server.impl.standalone.repo

import com.rollong.auth.common.UserIdentifier
import com.rollong.auth.common.session.UserSession
import com.rollong.auth.server.repo.SessionRepository
import org.springframework.stereotype.Component
import java.util.*
import java.util.concurrent.ConcurrentHashMap

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2021-01-04 16:23
 * @project rollong-auth
 * @filename StandaloneSessionRepositoryImpl.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.auth.server.impl.standalone.repo
 * @description
 */
@Component
class StandaloneSessionRepositoryImpl : SessionRepository {

  companion object {
    val storage: MutableMap<NamespaceAndId, UserSession> = ConcurrentHashMap()
  }

  override fun get(namespace: String, id: String): Optional<UserSession> {
    val key = NamespaceAndId(namespace, id)
    val session = storage[key]
    return if (null == session) {
      Optional.empty()
    } else if (session.logoutAt == null) {
      Optional.of(session)
    } else {
      Optional.empty()
    }
  }

  override fun revoke(namespace: String, id: String) {
    val key = NamespaceAndId(namespace, id)
    storage.remove(key)
  }

  override fun save(namespace: String, session: UserSession) {
    val key = NamespaceAndId(namespace, session.id)
    storage[key] = session
  }

  override fun touch(namespace: String, id: String) {
    val key = NamespaceAndId(namespace, id)
    storage[key]?.let {
      it.lastActivity = System.currentTimeMillis()
    }
  }

  override fun search(namespace: String, userIdentifier: UserIdentifier): List<UserSession> {
    return storage.filter { it.key.namespace == namespace && it.value.user == userIdentifier && it.value.logoutAt == null }.values.toList()
  }
}