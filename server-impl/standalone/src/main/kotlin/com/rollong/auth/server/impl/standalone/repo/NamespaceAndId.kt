package com.rollong.auth.server.impl.standalone.repo

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2021-01-04 16:24
 * @project rollong-auth
 * @filename NamespaceAndId.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.auth.server.impl.standalone.repo
 * @description
 */
data class NamespaceAndId(
    var namespace: String = "",
    var id: String = ""
)