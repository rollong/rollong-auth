package com.rollong.auth.server.impl.standalone.repo

import com.rollong.auth.server.repo.SessionRepository
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2021-01-24 10:29
 * @project rollong-auth
 * @filename CleanupScheduler.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.auth.server.impl.standalone.repo
 * @description
 */
@Component
class CleanupScheduler(
    private val sessionRepository: SessionRepository
) {

  @Scheduled(fixedDelay = 2 * 3600 * 1000L)
  fun cleanup() {
    val now = System.currentTimeMillis()
    StandaloneTokenRepositoryImpl.storage.filter { it.value.expiresAt <= now }.forEach {
      StandaloneTokenRepositoryImpl.storage.remove(it.key)
      this.sessionRepository.revoke(namespace = it.key.namespace, id = it.value.sessionId)
    }
  }
}