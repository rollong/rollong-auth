package com.rollong.auth.server.impl.standalone.repo

import com.rollong.auth.common.oauth2.authorizationCode.AuthorizationCode
import com.rollong.auth.server.repo.AuthorizationCodeRepository
import org.springframework.stereotype.Component
import java.util.*
import java.util.concurrent.ConcurrentHashMap

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2021-01-04 16:12
 * @project rollong-auth
 * @filename StandaloneAuthorizationCodeRepositoryImpl.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.auth.server.impl.standalone.repo
 * @description
 */
@Component
class StandaloneAuthorizationCodeRepositoryImpl : AuthorizationCodeRepository {

  companion object {
    val storage: MutableMap<NamespaceAndId, AuthorizationCode> = ConcurrentHashMap()
  }

  override fun get(namespace: String, code: String): Optional<AuthorizationCode> {
    val key = NamespaceAndId(namespace, code)
    val result = storage[key]
    return if (null == result) {
      Optional.empty()
    } else if (result.expiresAt > System.currentTimeMillis()) {
      storage.remove(key)
      Optional.of(result)
    } else {
      storage.remove(key)
      Optional.empty()
    }
  }

  override fun save(namespace: String, code: AuthorizationCode) {
    val key = NamespaceAndId(namespace, code.code)
    storage[key] = code
  }
}