package com.rollong.auth.server.impl.standalone.repo

import com.rollong.auth.common.token.Token
import com.rollong.auth.server.repo.TokenRepository
import org.springframework.stereotype.Component
import java.util.*
import java.util.concurrent.ConcurrentHashMap

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2021-01-04 16:37
 * @project rollong-auth
 * @filename StandaloneTokenRepositoryImpl.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.auth.server.impl.standalone.repo
 * @description
 */
@Component
class StandaloneTokenRepositoryImpl : TokenRepository {

  companion object {
    val storage: MutableMap<NamespaceAndId, Token> = ConcurrentHashMap()
  }

  override fun get(namespace: String, id: String): Optional<Token> {
    val key = NamespaceAndId(namespace, id)
    val result = storage[key]
    return if (null == result) {
      Optional.empty()
    } else if (result.expiresAt > System.currentTimeMillis()) {
      Optional.of(result)
    } else {
      storage.remove(key)
      Optional.empty()
    }
  }

  override fun revoke(namespace: String, id: String) {
    val key = NamespaceAndId(namespace, id)
    storage.remove(key)
  }

  override fun save(namespace: String, token: Token) {
    val key = NamespaceAndId(namespace, token.id)
    storage[key] = token
  }
}