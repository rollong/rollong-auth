package com.rollong.auth.server.impl.standalone

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.ComponentScan
import org.springframework.scheduling.annotation.EnableScheduling

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2021-01-04 16:08
 * @project rollong-auth
 * @filename ServerApplication.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.auth.server.impl.standalone
 * @description
 */
@SpringBootApplication
@EnableScheduling
@ComponentScan(basePackages = ["com.rollong.auth.server"])
class StandaloneServerApplication

fun main(args: Array<String>) {
  runApplication<StandaloneServerApplication>(*args)
}